FROM ubuntu:21.10

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -qq -y --no-install-recommends \
    dcmtk \
    dicom3tools \
    vtk-dicom-tools

RUN apt-get clean

RUN mkdir /input/

WORKDIR /input/
ENTRYPOINT [ "dicomdump" ]
