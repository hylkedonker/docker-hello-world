# docker-hello-world
Hello World Docker application that prints DICOM header. To run, mount your DICOM data to the /input/ directory:
```Bash
docker run \
	--volume /path/to/dicom/:/input/ \
	registry.gitlab.com/hylkedonker/docker-hello-world \
	my-file.dcm
```
